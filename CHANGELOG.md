# Verion 0.7.4

- Fixed text color and background on a few remaining windows

- Fixed progress indicator in Database Generator (now displays completion)

- Fixed: "Save Connection" and "Save Last Played" not working in certain circumstances

- Updatede to find Max 9 folder in Documents when database path set to Default

- Fixed microtonal display controls on Score page:
	- added a seperate control for bach.score
	- both controls now always reflect display

- Fixed: playback of anything stops when an orchestration is started

- Added horizontal zoom to bach.roll and bach.score on Score window

- Fixed: yml file no longer saved when outfiles off (ex. when only outbuffs on), because no yml data is saved, so file always empty

- Removed "autogroup" from Score section - no longer functional

- Improved explanation of richness parameter

- On Score page, made Set Score button easier to find

- Changed parameter in standalone object - turned on "Search for Missing Files." Without, apps built with 9 were unable to find subpatchers

- Updated app icon

- Changed buttons to activate on mouse up

# Verion 0.7.3

- Added : popups to explain each parameter

- Adjusted window location for new macbooks with taller menu bars

- Fixed potential with dropdowns: they had drag and drop enabled, so possible to repopulate contents by dropping a folder on them.

- Fixed: comments that display folder paths tend to change width, now set manually

- Fixed: updated flashing colors to white/red in File/Folders Locations dialog, after reset to default button on right clicked.

### Version 0.7.2b: new skin

# Version 0.7.1 : Adaptation for the new package Orchidea 0.7

- Cancelled : TinySOL, cause : depricated

- Added : Arrow in score window

- Added : partialsfilteringthresh

- Added : creation rate and deletion rate

- Changed : orchidea.solve replaced by orchidea.orchestrate

- Changed : orchidea.solution.toroll replaced by orchidea.toroll

- Changed : positive and negative penalization replaced by richness (layout and pattr storage)

- Changed : Description in "more about parameters"

- Changed : patialsfiltering is now a bool

- Changed : Display layout improved

- Added : tonedivision control

- Added : Create Databases

- Added : Effects 

- Added : Dictionary

- Added : New parameters 

# Version 0.6.1

- Added : Databases generator

- Changed : Display layout improved

- Changed : Status display

